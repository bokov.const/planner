# Planner

Simple events planner

## Description

The Planner takes as input a list of events, in which the event is a simple JSON structure. 

The structure has fields with the time at which this event will be implemented. 

When the required time comes, the event will be sent to the client.
