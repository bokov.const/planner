#ifndef INTERFACES_H
#define INTERFACES_H

#include <iostream>

#include <QDateTime>
#include <QDebug>

#define MAIN_FREQUENCY 10

class AbstractDataWorkerInterface
{
public:

    virtual ~AbstractDataWorkerInterface() noexcept {}

    // initialising
    virtual void init() = 0;
    // resolve incoming data
    virtual void handleData(const QString &tag, const QVariant &incomingData) = 0;
    // send request for data, for future, not in use now
    virtual void requestData(const QString &tag, const QVariantMap &parameters = QVariantMap()) = 0;
    // send data to folowers
    virtual void sendData(const QString &tag, const QVariant &data) { Q_UNUSED(tag); Q_UNUSED(data); }

    // tags api
    QStringList getTags() const { return this->tags; }
    void setTags(const QStringList &tags) { this->tags = tags; }

protected:
    QStringList tags;
};

// cpp interface for insert logging to class
class LoggingInterface {
public:

    enum LogLevels {info, warning, error, fail};

    LoggingInterface() {
        // set default log function
        this->log = [&](std::string mes, std::string tag, LogLevels level){
            std::time_t result = std::time(nullptr);
            std::cout << std::asctime(std::localtime(&result)) <<" "<< level <<" "<< mes <<" "<< tag << std::endl;
        };
    }

    // function for logging
    std::function<void(std::string, std::string, LogLevels)> log = nullptr;
    void setlog(std::function<void (std::string, std::string, LogLevels)> &value)
    {
        log = value;
    }
};

// abstruct data worker interface + qt signal/slots
class QObjectDataWorkerInterface : public QObject, public AbstractDataWorkerInterface
{
    Q_OBJECT
public:
    virtual ~QObjectDataWorkerInterface() noexcept {}

public slots:
    virtual void handleData(const QString &tag, const QVariant &incomingData) = 0;
    virtual void init() {}

signals:
    virtual void requestData(const QString &tag, const QVariantMap &parameters = QVariantMap());
    virtual void sendData(const QString &tag, const QVariant &data);
};

// astract timer interface
class AbstractTimerHandlerInterface
{
public:
    AbstractTimerHandlerInterface() : receptionFrequency(1){}

    virtual ~AbstractTimerHandlerInterface() noexcept {}
    virtual void timeOut() = 0;

    void changeFrequency(const int &secs) {
        if(secs <= 0)
            receptionFrequency = MAIN_FREQUENCY;
        else
            receptionFrequency = secs;
    }

    int getFrequency() const {
        return receptionFrequency;
    }

    bool isItRatio(const QTime &time) const {
        return !(time.second() % receptionFrequency);
    }

    bool isItRatio(const QDateTime &dateTime) const {
        return isItRatio(dateTime.time());
    }

private:
    int receptionFrequency;
};

#endif // INTERFACES_H


