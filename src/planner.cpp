#include "planner.h"

#include <iostream>
#include <string>

#include <QCryptographicHash>
#include <QByteArray>

#include <functional>

#include <QDebug>
#include <QTime>
#include <QDateTime>


DatedPlanner::DatedPlanner(QStringList keys)
    : EventsKeeper<QDateTime, QVariantMap>( [keys](const QVariantMap &map)->std::string {

    // if inputs keys array is empty, then all values are important:
    QStringList tags;
    if (keys.isEmpty())
        tags = map.keys();
    else
        tags = keys;

    QStringList specificData;
    for (const auto & tag : qAsConst(tags)) {
        specificData.append(map.value(tag).toString());
    }

    std::string key = QCryptographicHash::hash(specificData.join("").toUtf8(), QCryptographicHash::Md4).toHex().toStdString();
    return key;
})
{}

void DatedPlanner::push(const QVariantMap &data, const QList<QDateTime> &times)
{
    this->EventsKeeper::push(data, std::list<QDateTime>(times.begin(), times.end()));
}

std::vector <QVariantMap> DatedPlanner::pop(const QDateTime &datetime)
{
    std::vector <QVariantMap> returnValues;

    auto dates = getDatesBefore(datetime);

    for (const auto &date : qAsConst(dates) ) {
        auto values = this->EventsKeeper::pop(date);
        for (auto value : values) {
            if (std::find(returnValues.begin(), returnValues.end(), value) == returnValues.end() ) {
                returnValues.push_back(value);
            }
        }
    }

    return returnValues;
}

QList<QVariantMap> DatedPlanner::pull(const QDateTime &value)
{
    auto data = this->DatedPlanner::pop(value);
    // shit code, but ...
    return QList<QVariantMap>::fromVector(QVector<QVariantMap>(data.begin(), data.end()));
}

QList<QVariantMap> DatedPlanner::dataCopy() const
{
    auto data = this->DatedPlanner::getDataCopy();
    // shit code, but ...
    return QList<QVariantMap>::fromVector(QVector<QVariantMap>(data.begin(), data.end()));
}

void DatedPlanner::removeLast()
{
    auto last = this->provider.back();
    this->deleteFromProvider(std::get<0>(last), std::get<1>(last));
}

QList<QDateTime> DatedPlanner::getDatesBefore(const QDateTime &datetime) const
{
    QList<QDateTime> dates;

    auto it = provider.begin();
    while (it != provider.end() ) {
        auto date = std::get<0>(*it);
        if (date <= datetime ) {
            dates.append(date);
        } else {
            break;
        }
        it ++;
    }
    return dates;
}

//
// NotifyWorker class implementation
//

EventWorker::EventWorker(const QString &notify, const QStringList &watchingTags, const QStringList &targetTags, const int &updateInterval)
    : DatedPlanner(targetTags), LoggingInterface(), tag(notify), watchingKeys(watchingTags), updateInterval(updateInterval)
{
}

void EventWorker::push(const QVariantMap &data)
{
    QList<QDateTime> times;

    // for first data sending. If watchingTags is empty, data will be sended once
    // TODO: add this option to config
    times.append(QDateTime::currentDateTime());

    // get values of observable tags in incoming data
    for (const auto &tag : qAsConst(this->watchingKeys) ) {
        QDateTime time = data.value(tag).toDateTime();
        if (!time.isValid()) {
            log("datatime is not valid! ", LOG_TAG, LogLevels::info);
            continue;
        }
        times.append(time);
    }

    // add data
    this->DatedPlanner::push(data, times);

    // remove oldest data from container
    // TODO: rework it, too straight forward
    if (this->container.size() > this->maxSize) {
        this->removeLast();
    }
}

void EventWorker::push(const QList<QVariantMap> &data)
{
    for (const auto &map : data)
        this->push(map);
}

void EventWorker::push(const QVariantList &data)
{
    for (const auto &map : data)
        this->push(map.toMap());
}

void EventWorker::handleData(const QString &tag, const QVariant &incomingData)
{
    Q_UNUSED(tag);

    // check incoming data
    if (!incomingData.isValid()) {
        log(QString("invalid incoming data %1").arg(this->tag).toStdString(), LOG_TAG, LogLevels::info);
        return;
    }

    auto data = qvariant_cast<QList<QVariantMap>>(incomingData);

    // skip empty data
    if (data.isEmpty()) {
        log(QString("data is empty. maybe bad cast %1").arg(this->tag).toStdString(), LOG_TAG, LogLevels::info);
        return;
    }

    // adding event data
    push(data);
}

int EventWorker::getUpdateInterval() const
{
    return updateInterval;
}

void EventWorker::setUpdateInterval(int value)
{
    updateInterval = value;
}

//
// NotifyFilter class implementation
//

Planner::Planner(std::shared_ptr<QTimer> timer,
                 const QList<QVariantMap> &eventsProperties,
                 int timerFrequency) : LoggingInterface(), planScheme(eventsProperties)
{
    QObject::connect(timer.get(), &QTimer::timeout,
                     this, &Planner::timeOut);

    installFilters();
    this->changeFrequency(timerFrequency);
}

void Planner::installFilters()
{
    auto removeSpaces = [](const QStringList &keys)->QStringList{
        QStringList trimmedKeys;
        for (const auto &key : keys) {
            QString targetKey = key.trimmed();
            if (!targetKey.isEmpty())
                trimmedKeys.append(key.trimmed());
        }
        return trimmedKeys;
    };

    for (const auto & notify : qAsConst(planScheme)) {
        QString name = notify.value("event_tag").toString();

        // ignoring 'delete' notifications
        if (name.left(7) == "_delete") {
            continue;
        }

        // TODO:
        // time_keys and primary_keys must be an array, not a string
        QStringList watchingKeys = removeSpaces( notify.value("time_keys").toString().trimmed().split(",") );
        QStringList targetKeys = removeSpaces( notify.value("primary_keys").toString().trimmed().split(",") );
        int interval = notify.value("interval").toInt();

        if (!interval) {
            // TODO: add log
            interval = 10;
        }

        auto nextWorker = std::make_shared<EventWorker>(name, watchingKeys, targetKeys, interval);

        eventsWorkers.insert(name, nextWorker);
    }
}

bool Planner::isItRatio(const QTime &time, const int &ratio) const
{
    return !(time.second() % ratio);
}

void Planner::handleData(const QString &tag, const QVariant &incomingData)
{
    // handle exist of notification, this can happen in test, not in real work
    if (!this->eventsWorkers.contains(tag)) {
        log(QString("error: worker with %1 not found").arg(tag).toStdString(), LOG_TAG, LogLevels::info);
        return;
    }

    // convert
    auto data = qvariant_cast<QList<QVariantMap> >(incomingData);

    auto update_ = [&](QString tag){
        auto worker = eventsWorkers[tag];
        if (worker != nullptr) {
            worker->handleData(tag, incomingData);
        }
    };

    auto delete_ = [&](QString tag){
        // delete from worker (without '_delete' postfix)
        auto worker = eventsWorkers[tag.chopped(7)];
        if (worker != nullptr) {
            for (auto const & map : qAsConst(data))
                worker->EventsKeeper::remove(map);
        }
        // sending delete notification
        emit sendData(tag, incomingData);
    };

    if (tag.right(7) == "_delete") {
        delete_(tag);
    } else {
        update_(tag);
    }
}

void Planner::timeOut()
{
    auto time = QDateTime::currentDateTime().time();
    for (auto filter : qAsConst(eventsWorkers)) {

        // skip out-of-time filters
        if (!isItRatio(time, filter->getUpdateInterval())) {
            continue;
        }

        // if the filter has data, send it to client
        auto targetData = filter->pull(QDateTime::currentDateTime());

        // send data to clients
        if (!targetData.isEmpty()) {
            auto tag = eventsWorkers.key(filter);
            emit sendData(tag, QVariant::fromValue(targetData));
        }
    }
}
