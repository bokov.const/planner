#include <QObject>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QDateTime>

#include "planner.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // next code is simple example

    auto timer = std::make_shared<QTimer>();

    // filling plan scheme
    QList<QVariantMap> planScheme;
    planScheme.append(QVariantMap({{"event_tag", "session"},
                                   {"time_keys", "start, end"},
                                   {"primary_keys", "session_name"}
                                 }));

    planScheme.append(QVariantMap({{"event_tag", "enter"},
                                   {"time_keys", "start"},
                                   {"primary_keys", "id"}
                                 }));

    planScheme.append(QVariantMap({{"event_tag", "out"},
                                   {"time_keys", "time"},
                                   {"primary_keys", ""}
                                 }));

    // planner
    Planner *planner = new Planner(timer, planScheme);

    // way to install external logger,
    planner->log = [](std::string mes, std::string tag, int level) {
        // just for example
        qDebug() << QDateTime::currentDateTime() << " " << QString::fromStdString(mes) << " " << QString::fromStdString(tag) << " " << level ;
    };

    // callback for in-time event
    QObject::connect(planner, &Planner::sendData,
                     [](const QString &tag, const QVariant &data){
        qDebug() << QString("here is event [%1] ").arg(tag) << data;
    });

    // creation of test events set
    QList<QVariantMap> plan;

    QVariantMap event;
    event["start"] = QDateTime::currentDateTime().addSecs(rand() % 10);
    event["end"] = QDateTime::currentDateTime().addSecs(10 + rand() % 10);
    event["session_name"] = QString("session");
    plan.append(event);
    event["start"] = QDateTime::currentDateTime().addSecs(rand() % 10);
    event["end"] = QDateTime::currentDateTime().addSecs(10 + rand() % 10);
    event["session_name"] = QString("session");
    plan.append(event);

    // start of plan working
    planner->handleData(QString("session"), QVariant::fromValue(plan));

    timer->start(1);

    return a.exec();
}
