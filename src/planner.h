#ifndef PLANNER_H
#define PLANNER_H

#include <QObject>
#include <QTimer>

#include "interfaces.h"
#include "eventskeeper.h"

#define LOG_TAG "notify_worker"
#define LOG_TAG_COUNTER "notify_worker_counter"

// cache provader implementation with qt strutcs
class DatedPlanner : public EventsKeeper<QDateTime, QVariantMap>
{
public:
    DatedPlanner(QStringList keys);

    virtual void push(const QVariantMap &data, const QList<QDateTime> &times);

    virtual QList<QVariantMap> pull(const QDateTime &value);
    virtual QList<QVariantMap> dataCopy() const;

    virtual void removeLast();

protected:
    virtual std::vector<QVariantMap> pop(const QDateTime &datetime) override;
    virtual QList<QDateTime> getDatesBefore(const QDateTime &datetime) const;
};

// dated container with command-qobject interfaces
class EventWorker : public QObjectDataWorkerInterface,
        virtual public DatedPlanner,
        public LoggingInterface
{
    Q_OBJECT
public:
    explicit EventWorker(const QString &notify,
                          const QStringList &watchingTags,
                          const QStringList &targetTags,
                          const int &updateInterval = 10);

    // add event data
    void push(const QVariantMap &data);
    void push(const QList<QVariantMap> &data);
    void push(const QVariantList &data);

    // methods for controling update interval
    int getUpdateInterval() const;
    void setUpdateInterval(int value);

public slots:
    // method for resolve incoming data
    void handleData(const QString &tag, const QVariant &incomingData) override ;

private:
    int maxSize = 1000; // todo: move to config
    int updateInterval;

    // own tag
    QString tag;
    QStringList watchingKeys;
};

// class for control notify-workers
class Planner : public QObjectDataWorkerInterface,
        public AbstractTimerHandlerInterface,
        public LoggingInterface
{
    Q_OBJECT
public:
    Planner(std::shared_ptr<QTimer> timer,
            const QList<QVariantMap> &planScheme,
            int timerFrequency = 1);

    ~Planner() {}

public slots:

    // registration of plan (incomingData = plan with future events)
    void handleData(const QString &tag, const QVariant &incomingData) override;
    // resolve timer events
    void timeOut() override;

private:
    // installation of event workers
    void installFilters();
    // just function for check time gap
    bool isItRatio(const QTime &time, const int &ratio) const;

    // scheme of plan
    QList<QVariantMap> planScheme;
    // container with workers, one worker one tag
    QMap<QString, std::shared_ptr<EventWorker>> eventsWorkers;

signals:
    void requestData(const QString &tag, const QVariantMap &parameters = QVariantMap());
    void sendData(const QString &tag, const QVariant &data);
};

#endif // PLANNER_H

