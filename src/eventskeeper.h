#ifndef EVENTSKEEPER_H
#define EVENTSKEEPER_H

#include <iostream>
#include <functional>

#include <map>
#include <list>
#include <set>
#include <vector>
#include <algorithm>

// core for planner
template <class Provider, class Event>
class EventsKeeper
{
public:
    using KeyGeneratorType = std::function<std::string(const Event &)>;

    EventsKeeper(KeyGeneratorType keyGenerator) : keyGenerator(keyGenerator) {}

    // add new event
    virtual void push(const Event &event, const std::list<Provider> &providerValues) {

        std::string key = keyGenerator(event);

        // lazy data updating
        if (container.count(key)) {
            this->remove(key);
            push(event, providerValues);
            return;
        }

        // add data to container and provider
        for (const auto & value : providerValues) {
            provider.push_back( {value, key} );
        }
        container.insert({key, event});

        // sort provider by value
        std::sort(provider.begin(), provider.end(), [](const std::tuple <Provider, std::string> &first,
                                                       const std::tuple <Provider, std::string> &second)->bool {
            return std::get<0>(first) < std::get<0>(second);
        });
    }

    // get data by event key (provider)
    virtual std::vector <Event> pop(const Provider &value) {

        // get keys for input value
        auto keys = findKeys(value);

        std::vector <Event> foundData;

        for (const auto & key : keys) {

            auto iterator = container.find(key);

            // do not remove next comment
            // if (iterator == container.end()) {/*is it possible?*/ std::cout <<"something went wrong, container has no the key: " << key <<std::endl;}

            // add data to return struct
            foundData.push_back(std::get<1>(*iterator));

            // delete the data from provider
            deleteFromProvider(value, key);

            // check existence of the key in provider. If provider not contains the key, remove data from the container
            auto count = std::count_if(provider.begin(), provider.end(), [key](std::tuple <Provider, std::string> &line)->bool{
                    return (std::get<1>(line) == key);
            });

            // if event is not in demand, remove it
            if (!count) {
                container.erase(key);
            }
        }

        return foundData;
    }

    // get sizes of both, event and provider containers
    virtual std::tuple<int, int> getSizes() const {
        return { provider.size(), container.size() };
    }

    // remove event
    virtual void remove(const Event &data) {
        std::string key = keyGenerator(data);
        remove(key);
    }

    // get copy of container with events
    virtual std::vector <Event> getDataCopy() const {
        std::vector<Event> res;
        auto it = container.begin();
        while (it != container.end()) {
            res.push_back(std::get<1>(*it));
            it ++;
        }
        return res;
    }

protected:

    // return set of keys by provider value
    virtual std::set<std::string> findKeys (const Provider &value) const {
        std::set<std::string> keys;
        auto it = provider.begin();

        while (it != provider.end()) {
            if ( std::get<0>(*it) == value ) {
                keys.insert(std::get<1>(*it));
            }
            it ++;
        }
        return keys;
    }

    // remove all entries of data by the key
    virtual void remove(const std::string & key) {
        container.erase(key);
        deleteFromProvider(key);
    }

    // delete ALL LINES with the key
    void deleteFromProvider(const std::string &key) {
        provider.erase(std::remove_if(provider.begin(), provider.end(), [&key](std::tuple <Provider, std::string> line)->bool{
            std::string tmp = std::get<1>(line);
            return tmp == key;
        }), provider.end());
    }

    // delete one line from provider
    void deleteFromProvider(const Provider &value, const std::string &key) {
        provider.erase(std::remove_if(provider.begin(), provider.end(), [&key, &value](std::tuple <Provider, std::string> line)->bool{
            std::tuple<Provider, std::string> newTuple = {value, key};
            return (newTuple == line);
        }), provider.end());
    }

    // pointer for future generation of primary keys
    KeyGeneratorType keyGenerator = 0;

    std::map <std::string, Event> container;
    std::vector <std::tuple <Provider, std::string> > provider;

};

#endif // EVENTSKEEPER_H
